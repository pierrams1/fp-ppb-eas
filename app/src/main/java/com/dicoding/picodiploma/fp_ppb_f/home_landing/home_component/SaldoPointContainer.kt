package com.dicoding.picodiploma.fp_ppb_f.home_landing.home_component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun SaldoPointContainer(){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(120.dp)
            .padding(14.dp)
            .border(BorderStroke(1.dp, Color.Gray), RoundedCornerShape(10.dp))
            .wrapContentHeight(Alignment.CenterVertically)
    ){
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .width(190.dp)
                .padding(12.dp)
                .wrapContentHeight(Alignment.CenterVertically)
        ){
            Text(
                text = "Saldo",
                fontSize = 16.sp,
                fontWeight = FontWeight.Light
            )
            Text(
                text = "Rp100.000,00-",
                fontSize = 20.sp,
                fontWeight = FontWeight.Medium

            )
        }

        Divider(
            color = Color.Gray,
            modifier = Modifier
                .fillMaxHeight()
                .width(1.dp)
        )

        Column(
            modifier = Modifier
                .fillMaxHeight()
                .width(150.dp)
                .padding(12.dp)
                .wrapContentHeight(Alignment.CenterVertically)
        ){
            Text(
                text = "Point",
                fontSize = 16.sp,
                fontWeight = FontWeight.Light
            )
            Text(
                text = "100 Point",
                fontSize = 20.sp,
                fontWeight = FontWeight.Medium
            )
        }
    }
}