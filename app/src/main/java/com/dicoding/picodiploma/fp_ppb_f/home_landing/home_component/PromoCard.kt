package com.dicoding.picodiploma.fp_ppb_f.home_landing.home_component

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import com.dicoding.picodiploma.fp_ppb_f.R
import com.dicoding.picodiploma.fp_ppb_f.component.CustomButton
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Composable
fun PromoCard(){
    var closeState by remember { mutableStateOf(false) }
    val formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy")
    var date = LocalDateTime.now().format(formatter)

    ElevatedCard(modifier = Modifier
        .zIndex(if (closeState) 0F else 2F)
        .fillMaxSize(), colors = CardDefaults.cardColors(Color.Black.copy(alpha = 0.6F)),){}
    ElevatedCard(
        modifier = Modifier
            .zIndex(if (closeState) 0F else 2F)
            .fillMaxSize()
            .padding(horizontal = 20.dp)
            .padding(bottom = 60.dp, top = 60.dp),
        colors = CardDefaults.cardColors(Color(11, 66, 26))
    ){
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(64.dp)
                .wrapContentHeight(Alignment.CenterVertically)
                .padding(horizontal = 12.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ){
            Text(text = "Starbuck Reward", color = Color.White, fontSize = 18.sp)
            Image(
                painter = painterResource(id = R.drawable.close),
                contentDescription = "close",
                modifier = Modifier.clickable {
                    closeState = true
                }
            )
        }

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .weight(0.5F)
                .wrapContentWidth(Alignment.CenterHorizontally),
            verticalArrangement = Arrangement.SpaceBetween
        ){
            Text(
                text = "Selamat Datang di Starbucks",
                color = Color.White,
                fontSize = 36.sp,
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.fillMaxWidth(),
                lineHeight = 48.sp
            )
            Column {
                Text(
                    text = "Kamu mendapatkan Promo Minuman Rp10.000 untuk 1 kopi yang kamu beli hanya untuk hari ini",
                    color = Color.White,
                    textAlign = TextAlign.Center,
                    fontSize = 16.sp,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 18.dp)
                        .padding(top = 12.dp),
                )
                Spacer(modifier = Modifier.padding(4.dp))
                Text(
                    text = "Batas Pembelian pada ${date}",
                    color = Color.White,
                    fontSize = 12.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentWidth(Alignment.CenterHorizontally)
            ){
                Image(painter = painterResource(id = R.drawable.coffee_cup), contentDescription = "Coffee", modifier = Modifier.size(320.dp))
            }
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(120.dp)
                .wrapContentWidth(Alignment.CenterHorizontally)
                .wrapContentHeight(Alignment.Top)
        ){
            CustomButton(onClick = { closeState = true }, string = "Redeem", bgColor = Color(234, 199, 132), textColor = Color.White, 250)
        }
    }
}