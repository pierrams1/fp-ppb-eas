package com.dicoding.picodiploma.fp_ppb_f.component

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun AuthButton(onClick : () -> Unit, string: String, BgColor: Color, TextColor: Color){
    Button(
        modifier = Modifier
            .width(350.dp)
            .height(64.dp)
            .padding(top = 12.dp),
        shape = RoundedCornerShape(10.dp),
        colors = ButtonDefaults.buttonColors(containerColor = BgColor),
        onClick = onClick
    ) {
        Text(
            text = "$string",
            fontSize =16.sp,
            color = TextColor
        )
    }
}

@Composable
fun CustomButton(onClick : () -> Unit, string: String, bgColor: Color, textColor: Color, width: Int){
    Button(
        modifier = Modifier
            .width(width.dp)
            .height(64.dp)
            .padding(top = 12.dp),
        shape = RoundedCornerShape(10.dp),
        colors = ButtonDefaults.buttonColors(containerColor = bgColor),
        onClick = onClick
    ) {
        Text(
            text = "$string",
            fontSize = 20.sp,
            color = textColor
        )
    }
}
