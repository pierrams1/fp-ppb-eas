package com.dicoding.picodiploma.fp_ppb_f.home_landing.home_component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

@Composable
fun ProductContainer(num: Int){
    Column(
        modifier = Modifier
            .width(150.dp)
            .height(240.dp)
            .padding(10.dp)
            .border(BorderStroke(1.dp, Color.Gray), RoundedCornerShape(10.dp))
            .wrapContentHeight(Alignment.CenterVertically)
    ){
        Text(
            text = "Produk ${num}",
            modifier = Modifier
                .fillMaxSize()
                .wrapContentHeight(Alignment.CenterVertically),
            textAlign = TextAlign.Center
        )
    }
}