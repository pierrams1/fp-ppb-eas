package com.dicoding.picodiploma.fp_ppb_f.home_landing.home_component

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.dicoding.picodiploma.fp_ppb_f.R

@Composable
fun BottomBar(){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(80.dp)
            .background(color = Color(11, 66, 26))
            .padding(8.dp) ,
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceAround
    ){
        Image(painter = painterResource(id = R.drawable.home), contentDescription = "Home", modifier = Modifier.size(40.dp))
        Image(painter = painterResource(id = R.drawable.coffee), contentDescription = "Produk", modifier = Modifier.size(40.dp))
        Image(painter = painterResource(id = R.drawable.person), contentDescription = "Profil", modifier = Modifier.size(40.dp))
    }
}