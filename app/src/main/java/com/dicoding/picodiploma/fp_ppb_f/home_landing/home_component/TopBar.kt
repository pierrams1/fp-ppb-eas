package com.dicoding.picodiploma.fp_ppb_f.home_landing.home_component

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dicoding.picodiploma.fp_ppb_f.AuthViewModel
import com.dicoding.picodiploma.fp_ppb_f.R

@Composable
fun TopBar(authViewModel: AuthViewModel){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(72.dp)
            .background(color = Color(11, 66, 26))
            .padding(8.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ){
        Row(
            modifier = Modifier
                .height(72.dp),
            verticalAlignment = Alignment.CenterVertically
        ){
            Image(painter = painterResource(id = R.drawable.starbuck_background), contentDescription = "Logo")
            Text(
                text = "Starbucks",
                fontSize = 20.sp,
                modifier = Modifier.padding(start = 12.dp),
                color = Color(255,252,252)
            )
        }
        Image(
            painter = painterResource(id = R.drawable.logout),
            contentDescription = "Logout",
            modifier = Modifier.clickable {
                authViewModel.logout()
            }
        )
    }
}