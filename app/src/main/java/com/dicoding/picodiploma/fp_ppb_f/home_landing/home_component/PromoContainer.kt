package com.dicoding.picodiploma.fp_ppb_f.home_landing.home_component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dicoding.picodiploma.fp_ppb_f.R

@Composable
fun PromoContainer(){
    Text(
        text = "Promo",
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 10.dp, start = 14.dp),
        fontWeight = FontWeight.Medium,
        fontSize = 20.sp
    )
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp)
            .padding(14.dp)
            .border(BorderStroke(1.dp, Color.Gray), RoundedCornerShape(10.dp))
    ){
        Image(
            painter = painterResource(id = R.drawable.rectangle),
            contentDescription = "Promotion",
            modifier = Modifier
                .fillMaxSize()
                .clip(RoundedCornerShape(10.dp))
        )
    }
}