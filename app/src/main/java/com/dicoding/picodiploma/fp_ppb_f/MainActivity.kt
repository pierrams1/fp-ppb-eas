package com.dicoding.picodiploma.fp_ppb_f

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.dicoding.picodiploma.fp_ppb_f.home_landing.HomePage
import com.dicoding.picodiploma.fp_ppb_f.login.LoginPage
import com.dicoding.picodiploma.fp_ppb_f.register.RegisterPage
import com.dicoding.picodiploma.fp_ppb_f.ui.theme.FPPPBFTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FPPPBFTheme {
                AppScreen()
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun AppScreen() {
    FPPPBFTheme {
        val navController = rememberNavController()
        val authViewModel = AuthViewModel()
        NavHost(navController = navController, startDestination = "login_page") {
            composable("login_page"){
                LoginPage(navController, authViewModel)
            }

            composable("register_page"){
                RegisterPage(navController, authViewModel)
            }

            composable("home_page"){
                HomePage(navController, authViewModel)
            }
        }
    }
}

