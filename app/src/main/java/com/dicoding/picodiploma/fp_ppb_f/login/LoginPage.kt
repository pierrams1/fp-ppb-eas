package com.dicoding.picodiploma.fp_ppb_f.login

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.dicoding.picodiploma.fp_ppb_f.AuthState
import com.dicoding.picodiploma.fp_ppb_f.AuthViewModel
import com.dicoding.picodiploma.fp_ppb_f.R
import com.dicoding.picodiploma.fp_ppb_f.component.AuthButton

@Composable
fun LoginPage(navController: NavController, authViewModel: AuthViewModel){
    val authState = authViewModel.authState.observeAsState()
    val context = LocalContext.current

    LaunchedEffect(authState.value) {
        when(authState.value){
            is AuthState.Authenticated -> navController.navigate("home_page")
            is AuthState.Error -> Toast.makeText(context, (authState.value as AuthState.Error).message, Toast.LENGTH_SHORT).show()
            else -> Unit
        }
    }

    Surface {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 64.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ){
            var email by remember { mutableStateOf("") }
            var password by remember { mutableStateOf("") }

            Image(
                painter = painterResource(id = R.drawable.starbuck),
                contentDescription = "Logo",
                modifier = Modifier
                    .size(320.dp)
                    .padding(bottom = 24.dp)
            )

            OutlinedTextField(
                value = email,
                onValueChange = { email = it },
                label = { Text("Email") },
                modifier = Modifier
                    .width(350.dp)
                    .height(84.dp)
                    .padding(top = 6.dp, bottom = 6.dp),
                shape = RoundedCornerShape(10.dp),
                singleLine = true,
            )

            OutlinedTextField(
                value = password,
                onValueChange = { password = it },
                label = { Text("Password") },
                modifier = Modifier
                    .width(350.dp)
                    .height(84.dp)
                    .padding(top = 6.dp, bottom = 6.dp),
                shape = RoundedCornerShape(10.dp),
                singleLine = true,
                visualTransformation = PasswordVisualTransformation()
            )

            AuthButton({authViewModel.login(email, password)}, "Sign In", Color(11, 66, 26), Color(255,252,252))

            Row(
                modifier = Modifier.padding(top = 24.dp)
            ){
                Text(
                    text = "Do not have account? ",
                    fontSize = 14.sp
                )
                Text(
                    text = "Sign Up",
                    fontSize = 14.sp,
                    modifier = Modifier.clickable {
                        navController.navigate("register_page")
                    }
                )
            }
        }
    }
}