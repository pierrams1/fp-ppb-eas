package com.dicoding.picodiploma.fp_ppb_f.home_landing

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import androidx.navigation.NavController
import com.dicoding.picodiploma.fp_ppb_f.AuthState
import com.dicoding.picodiploma.fp_ppb_f.AuthViewModel
import com.dicoding.picodiploma.fp_ppb_f.home_landing.home_component.BottomBar
import com.dicoding.picodiploma.fp_ppb_f.home_landing.home_component.ProductContainer
import com.dicoding.picodiploma.fp_ppb_f.home_landing.home_component.PromoCard
import com.dicoding.picodiploma.fp_ppb_f.home_landing.home_component.PromoContainer
import com.dicoding.picodiploma.fp_ppb_f.home_landing.home_component.SaldoPointContainer
import com.dicoding.picodiploma.fp_ppb_f.home_landing.home_component.TopBar

@Composable
fun HomePage(navController: NavController, authViewModel: AuthViewModel){
    val authState = authViewModel.authState.observeAsState()
    val context = LocalContext.current

    LaunchedEffect(authState.value) {
        when(authState.value){
            is AuthState.Unauthenticated -> navController.navigate("login_page")
            is AuthState.Error -> Toast.makeText(context, (authState.value as AuthState.Error).message, Toast.LENGTH_SHORT).show()
            else -> Unit
        }
    }

    Surface {
        PromoCard()

        Column(
            modifier = Modifier
                .fillMaxSize()
                .zIndex(1F)
                .background(Color(255, 252, 252))
        ){
            TopBar(authViewModel)

            Column(
                modifier = Modifier.weight(0.5F)
            ){
                Text(
                    text = "Welcome to Starbucks",
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(80.dp)
                        .wrapContentHeight(Alignment.CenterVertically),
                    textAlign = TextAlign.Center,
                    fontSize = 36.sp,
                    fontWeight = FontWeight.ExtraBold,
                    color = Color(11, 66, 26)
                )

                SaldoPointContainer()

                PromoContainer()

                Text(
                    text = "Produk",
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 10.dp, start = 14.dp),
                    fontWeight = FontWeight.Medium,
                    fontSize = 20.sp
                )
                LazyRow {
                    items((1..10).toList()){
                        ProductContainer(it)
                    }
                }
            }

            BottomBar()
        }
    }
}

