package com.dicoding.picodiploma.fp_ppb_f.component

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun EmailTextField(){
    var email by remember { mutableStateOf("") }

    OutlinedTextField(
        value = email,
        onValueChange = { email = it },
        label = { Text("Email") },
        modifier = Modifier
            .width(350.dp)
            .height(84.dp)
            .padding(top = 6.dp, bottom = 6.dp),
        shape = RoundedCornerShape(10.dp),
        singleLine = true,
    )
}

@Composable
fun PasswordTextField(){
    var password by remember { mutableStateOf("") }

    OutlinedTextField(
        value = password,
        onValueChange = { password = it },
        label = { Text("Password") },
        modifier = Modifier
            .width(350.dp)
            .height(84.dp)
            .padding(top = 6.dp, bottom = 6.dp),
        shape = RoundedCornerShape(10.dp),
        singleLine = true,
    )
}